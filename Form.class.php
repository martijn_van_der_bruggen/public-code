<?php

class Form{
 
    
    private function sticky($fieldname,$msg = 'Please filll out all fields.'){
            
            $this->fieldname = filter_var($_POST[$fieldname],FILTER_SANITIZE_STRING);
            $this->msg =$msg;
            if(isset($this->fieldname) && (!empty($this->fieldname) )){return $this->fieldname;} else { return $this->msg;}        
            
            }
    
    
    public function show(){
        $this->fields;
        $this->fields .= '<div class="col-md-4"><br><br><br><form  role="form" action="" method="POST">';
        $this->fields .='<div class="form-group"><label for="s_functie">Functie:</label>';
        $this->fields .='<input type="text" class="form-control" name="s_functie" id="s_functie" '
                . 'value="'.$this->sticky('s_functie').'"></div>';
        $this->fields .='<div class="form-group"><label for="o_name">Organisatie:</label>';
        $this->fields .='<input type="text" class="form-control" name="o_name" id="o_name" '
                . 'value="'.$this->sticky('o_name').'"></div>';
        $this->fields .='<div class="form-group"><label for="o_phone">Phone:</label>';
        $this->fields .='<input type="text" class="form-control" name="o_phone" id="o_phone" '
                . 'value="'.$this->sticky('o_phone').'"></div>';
        $this->fields .='<div class="form-group"><label for="o_web">Website:</label>';
        $this->fields .='<input type="text" class="form-control" name="o_web" id="o_web" '
                . 'value="'.$this->sticky('o_web').'"></div>';
        $this->fields .='<div class="form-group"><label for="o_email">Email:</label>';
        $this->fields .='<input type="email" class="form-control" name="o_email" id="o_email" '
                . 'value="'.$this->sticky('o_email').'"></div>';
        $this->fields .='<input type="submit" name="submit" class="btn btn-default" value="submit"></form></div>';
        return $this->fields;
        
    }
}
