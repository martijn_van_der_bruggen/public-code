<?php

/* i ran into problems creating the foreign key relationship in database...Engine should be inno_db
 * REFERENCED column should be indexed and both colums should be of same datatype and size.
 * Dont think destroying the db object (set to null) is needed after return but cant hurt either :)
 *  Todo: implement transactions , implement traits and interfaces to reduce duplicated code, make stuff more generic
 * */
//final keyword prevents subclassing hence overriding access methods
final Class Db {
    
    public $params;
    
    public function __construct(){
        //just some experiment with paths
        include (dirname(dirname(__DIR__)).'/includes/credentials.inc.php');
        $this->db = new PDO('mysql:host=localhost;dbname=sollicitaties',$user,$pw);
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
    }
    
    public function insert(){
                
        try{
        //prepare data for database
        $this->params = array(
            "s_functie" => filter_var($_POST['s_functie'],FILTER_SANITIZE_STRING),
            "o_name" => filter_var($_POST['o_name'],FILTER_SANITIZE_STRING),
            "o_phone" => filter_var($_POST['o_phone'],FILTER_SANITIZE_STRING),
            "o_web" => filter_var($_POST['o_web'],FILTER_SANITIZE_URL),
            "o_email" => filter_var($_POST['o_email'],FILTER_SANITIZE_EMAIL),
        );
        //last inserted id koppelt het laaste id van sollicitatie tabel aan organisatie tabel       
        $sql ="INSERT INTO sollicitatie (functie, datum) VALUES(:s_functie, NOW());";
        $sql .="INSERT INTO organisatie (sollicitatie_id, o_name, o_phone, o_web, o_email) VALUES "
                . "( LAST_INSERT_ID(), :o_name, :o_phone, :o_web, :o_email );";
        //send the query to the database 
         $stmt = $this->db->prepare($sql);
         
        //bind variables(array) to the query and execute
        $stmt->execute($this->params);
        //close database connectie
        $this->db = NULL;
               
        } catch(PDOException $e){
        
        echo 'Connection failed: ' . $e->getMessage().'</div>';
                    
        }
              
    }
    public function listSol(){
           
        $sql ="SELECT s.functie,o.o_name,o.o_email FROM sollicitatie s JOIN organisatie  o ON s.sollicitatie_id=o.sollicitatie_id";
        $stmt =$this->db->prepare($sql);
        $stmt->execute();
        $this->html = '<table class="sollicitaties"><th>Sollicitaties:</th><tr>';
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            
           
            $this->html .='<td>'.$row['functie'].'</td><td>'.$row['o_name'].'</td><td>'.$row['o_email'].'</td>'
            . '<td>'.$row['datum'].'</td></tr>';
                    
        }
        $this->html .= '</table>';
        return $this->html;
       
        
        }
}
