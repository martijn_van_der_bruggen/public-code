<?php

class Menu {
    
    public function show(array $items,$menu_name){
        $this->menu = '<nav class="navbar navbar-inverse navbar-fixed-top">';
        $this->menu .= '<div class="container">';
        $this->menu .= '<div class="navbar-header"><div class="navbar-brand">Mijn Sollicitaties</div></div>';
        $this->menu .= '<ul class="nav navbar-nav">';
        
        foreach ($items as $item){
            $this->item = $item;
            $this->menu .= '<li><a href="'.$this->item.'.view.php">'.$this->item.'</a></li>';
            
        }
        $this->menu .= '</ul></div></nav>';
        return $this->menu;
        
        
        
    }
}
